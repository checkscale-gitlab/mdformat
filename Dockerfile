FROM python:3.9-slim

# hadolint ignore=DL3013
RUN pip --disable-pip-version-check --no-cache-dir install --upgrade \
        pip \
        setuptools \
        wheel \
    && \
    pip --disable-pip-version-check --no-cache-dir install \
        mdformat \
        mdformat-beautysh \
        mdformat-black \
        mdformat-config \
        mdformat-web

WORKDIR /src
ENTRYPOINT [ "mdformat" ]
CMD [ "--help" ]
