# mdformat

A Docker image for [executablebooks/mdformat](https://github.com/executablebooks/mdformat).

> Mdformat is an opinionated Markdown formatter that can be used to enforce
> a consistent style in Markdown files. Mdformat is a Unix-style command-line
> tool as well as a Python library.

Integrated plugins:

* mdformat-beautysh
* mdformat-black
* mdformat-config
* mdformat-shfmt

## Usage

The `WORKDIR` is `/src/`. You want to bind-mount your files there:

```shell
docker run --rm -u $(id -u):$(id -g) -v $(pwd):/src letompouce/mdformat
```

Example:

```text
$ docker run --rm -u $(id -u):$(id -g) -v $(pwd):/src letompouce/mdformat . --check
Error: File "/src/README.md" is not formatted
```

### GitLab CI/CD usage

Basic invocation:

```yaml
test:
  stage: test
  image:
    name: letompouce/mdformat
    entrypoint: [""]
  script:
    - mdformat --check README.md
```

See the
[.gitlab-ci.test-job.yml](https://gitlab.com/l3tompouce/docker/mdformat/-/blob/main/.gitlab-ci.test-job.yml)
file.

## Links

* Source: <https://gitlab.com/l3tompouce/docker/mdformat>
* Image: <https://hub.docker.com/r/letompouce/mdformat>
